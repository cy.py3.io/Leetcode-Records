package _817_Linked_List_Components;

import java.util.HashSet;

public class Test {
	public class ListNode {
	    int val;
	    ListNode next;
	    ListNode(int x) { val = x; }
	}
	public int numComponents(ListNode head, int[] G) {
		HashSet<Integer> Gset = new HashSet<>();
		for(int x:G) {
			Gset.add(x);
		}
		int count=0,flag=0;
		ListNode p = head;
		while(p!=null) {
			if(Gset.contains(p.val) && flag==0) {
				flag = 1;
				count++;
				p=p.next;
				continue;
			}
			if(Gset.contains(p.val) && flag==1) {
				p=p.next;
				continue;
			}
			if(!Gset.contains(p.val)) {
				p=p.next;
				flag=0;
			}
		}
		return count;
    }
}
