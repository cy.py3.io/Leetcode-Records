package _92_Reverse_Linked_List_II;

import java.awt.color.ICC_ColorSpace;
import java.util.concurrent.CountDownLatch;

import _109_Convert_Sorted_List_to_Binary_Search_Tree.Test.ListNode;

public class Test {
	public class ListNode {
	    int val;
	    ListNode next;
	    ListNode(int x) { val = x; }
	}
	public ListNode reverseBetween(ListNode head, int m, int n) {
        ListNode dummyhead = new ListNode(0);
        dummyhead.next = head;
        ListNode move=dummyhead, p1,pre,cur;
        int count = 0;
        while(move!=null) {
        	if(count==m-1) {
        		p1 = move;
        		pre = move.next;
        		cur = move.next.next;
        		move=move.next;
        		continue;
        	}
        	if(count>=m && count<n) {
        		cur.next=pre;
        		pre=cur;
        		cur=cur.next;
        		move=move.next;
        		continue;
        	}
        	if(count==n && m!=n) {
        		p1.next.next=pre.next;
        		p1.next=pre;
        		move=move.next;        		
        	}
        }
        return dummyhead.next;
    }
}
