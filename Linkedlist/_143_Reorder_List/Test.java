package _143_Reorder_List;

public class Test {
	public class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
    }
	public void reorderList(ListNode head) {
        if(head==null||head.next==null) return;
        ListNode slow = head, fast = head;
        while(fast.next!=null && fast.next.next!=null) {
        	slow=slow.next;
        	fast=fast.next.next;
        }
        
        ListNode revhead = slow,pre=slow.next,cur=pre.next;
        while(cur!=null) {
        	ListNode temp=cur.next;
        	cur.next=revhead.next;
        	revhead.next=cur;
        	pre.next=temp;
        	cur=temp;
        }
        
        ListNode p1=head,p2=revhead.next;
        while(p2!=null) {
        	ListNode tempp1 = p1.next,tempp2 = p2.next;
        	p1.next=p2;
            if(tempp2==null && p1==revhead){
                p2.next=null;
            }else if(tempp2==null && p1!=revhead){
                p2.next=tempp1;
                tempp1.next=null;
            }else{
                p2.next=tempp1;
            }
        	
        	p1 = tempp1;
        	p2 = tempp2;
        }              
    }
}
