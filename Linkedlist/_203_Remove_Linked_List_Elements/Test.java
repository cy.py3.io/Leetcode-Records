package _203_Remove_Linked_List_Elements;

import java.awt.List;

public class Test {
	public ListNode removeElements(ListNode head, int val) {
		if(head==null) return head;
        ListNode dummy = new ListNode(0);
		 ListNode p1=dummy,p2=head;
		 while(p2!=null) {
             if(p2.val==val) {
                p2=p2.next;
             }else {
                 p1.next=p2;
                 p1=p2;
                 p2=p2.next;
             }
		 }
		 p1.next=null;
		 return dummy.next;
    }
}
