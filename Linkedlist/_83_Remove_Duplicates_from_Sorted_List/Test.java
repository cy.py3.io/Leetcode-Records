package _83_Remove_Duplicates_from_Sorted_List;

import _143_Reorder_List.Test.ListNode;

public class Test {
	public class ListNode {
	      int val;
	      ListNode next;
	      ListNode(int x) { val = x; }
	}
	 public ListNode deleteDuplicates(ListNode head) {
		 if(head==null||head.next==null) return head;
		 ListNode p1=head,p2=head.next; 
		 while(p2!=null) {
			 if(p2.val==p1.val) {
				 p2=p2.next;
			 }else {
				 p1.next=p2;
				 p1=p2;
				 p2=p2.next;
			 }
		 }
		 p1.next=null;
		 return head;
	 }
}
