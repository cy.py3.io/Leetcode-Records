package _109_Convert_Sorted_List_to_Binary_Search_Tree;

public class Test {
  public class ListNode {
     int val;
     ListNode next;
     ListNode(int x) { val = x; }
  }
		 
		
	
  public class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
  }
	public TreeNode sortedListToBST(ListNode head) {
        if(head==null) return null;
        return toBST(head,null);
    }
	private TreeNode toBST(ListNode head,ListNode tail) {
		ListNode fast=head, slow=head;
		if(head==tail) return null;
		while(fast!=tail && fast.next!=tail) {
			slow=slow.next;
			fast=fast.next.next;
		}
		TreeNode thead = new TreeNode(slow.val);
		thead.left = toBST(head, slow);
		thead.right = toBST(slow.next, tail);
		return thead;
	}
}
