package _725_Split_Linked_List_in_Parts;

public abstract class Test {
	public class ListNode {
	    int val;
	    ListNode next;
	    ListNode(int x) { val = x; }
	}
	public ListNode[] splitListToParts(ListNode root, int k) {
        ListNode[] ans = new ListNode[k];
        if(root==null) return ans;
        int length=0,num1,base;
        ListNode p1=root,pre;
        while(p1!=null) {
        	length++;
        	p1=p1.next;
        }
        base = length/k;
        num1 = length%k;
        pre = root;
        
        for(int i=0;i<k;i++) {
        	int times;
    		ans[i]= pre;
    		if(num1-->0) {
    			times = base+1;
    		}else {
    			times = base;
    		}
    		p1=pre;
    		while(--times>0) {
    			p1=p1.next;
    		}
            if(base==0 && num1==0)break;
    		if(i!=k-1){
                pre=p1.next;
    		    p1.next=null;
            }
        }
        return ans; 
    }
}
