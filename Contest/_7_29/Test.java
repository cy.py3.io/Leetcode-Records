package _7_29;

public class Test {
	public int nthMagicalNumber(int N, int A, int B) {
		long lcm = A*B/gcd(A, B);
		long loop = lcm/A+lcm/B-1;
		long ans1 = Math.floorDiv(N, loop) * lcm;
		long count = N%loop;
		while(count--!=0) {
			ans1+=Math.min(A, Math.min(B, Math.min(A-ans1%A, B-ans1%B)));
		}
		return (int)(ans1%(Math.pow(10, 9)+7));
		
    }
	public static long gcd(long x,long y){
        if(x<y)//判断x和y的大小
        {
            long temp=x;
            x=y;
            y=temp;
        }
        if(x%y==0){
            return y;
        }
        else{
            return gcd(y,x%y);//递归求最大公约数
        }
	}
	
	public static void main(String []arg) {
		Test aTest = new Test();
		System.out.println(aTest.nthMagicalNumber(5, 2, 4));
	}
	
	
}
