import java.util.logging.Handler;

public class _875_Koko_Eating_Bananas {
	public int minEatingSpeed(int[] piles, int H) {
        double sum=0,min=0,tempH=0;
        for(int i=0;i<piles.length;i++) {
        	sum+=piles[i];
        }
        min = Math.ceil(sum/H);
    
        
        while(true) {
        	for(int i=0;i<piles.length;i++) {
            	tempH+=Math.ceil(piles[i]/min); 
            }
        	if(tempH > H) {
        		min++;
        		tempH=0;
        	}else {
        		break;
        	}
        }
        return (int)min;      
    }
	public static void main(String []arg) {
		_875_Koko_Eating_Bananas a = new _875_Koko_Eating_Bananas();
		int[] piles = {30,11,23,4,20};
		int H = 6; 
		System.out.print(a.minEatingSpeed(piles, H));
	}
}
