package _873_Length_of_Longest_Fibonacci_Subsequence;

import java.util.HashSet;

public class Test {
	public int lenLongestFibSubseq(int[] A) {
        HashSet<Integer> set = new HashSet<>();
		for(int i=0;i<A.length;i++) {
			set.add(A[i]);
		}
		int max = 0,count=2;
		for(int i=0;i<A.length;i++) {
			for(int j=i+1;j<A.length;j++) {
				int pre=A[i],cur=A[j],sum=pre+cur;
				while(true) {
					if(set.contains(sum)) {
						pre = cur;
						cur = sum;
						sum = cur + pre;
						count++;
					}else {
						break;
					}
				}
                if(count>max) {
                    max=count;
                }
                count=2;
			}
		}
		return max==2?0:max;
    }
}
}
