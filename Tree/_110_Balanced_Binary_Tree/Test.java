package _110_Balanced_Binary_Tree;


public class Test {
	
	public class TreeNode {
	    int val;
	    TreeNode left;
	    TreeNode right;
	    TreeNode(int x) { val = x; }
	 }
	
	private int depth(TreeNode root) {
		if(root == null) return 0;
		return Math.max(depth(root.right), depth(root.left))+1;
	}
	
	public boolean isBalanced(TreeNode root) {
        if(root == null) return true;
        
        int leftdepth = depth(root.left);
        int rightdepth = depth(root.right);
        
        return (Math.abs(leftdepth-rightdepth)<=1 && isBalanced(root.left) && isBalanced(root.right));
    }
}
