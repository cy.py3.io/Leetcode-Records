package _100_Same_Tree;

import _617_Merge_Two_Binary_Trees.Test.TreeNode;

public class Test {
	public class TreeNode {
	    int val;
	    TreeNode left;
	    TreeNode right;
	    TreeNode(int x) { val = x; }
	 }
	public boolean isSameTree(TreeNode p, TreeNode q) {
        if(p==null && q==null) return true;
        if((p==null && q!=null) || (p!=null) && q==null)return false;
        return (p.val==q.val && isSameTree(p.left, q.left) && isSameTree(p.right, q.right));
    }
//	public boolean isSameTree(TreeNode p, TreeNode q) {
//	    if(p == null && q == null) return true;
//	    if(p == null || q == null) return false;
//	    if(p.val == q.val)
//	        return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
//	    return false;
//	}
}
