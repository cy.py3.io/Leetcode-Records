package _226_Invert_Binary_Tree;



public class Test {
	public class TreeNode {
	    int val;
	    TreeNode left;
	    TreeNode right;
	    TreeNode(int x) { val = x; }
	 }
	public TreeNode invertTree(TreeNode root) {
		if(root==null) return root;
        TreeNode tmp = root.left;
        root.left = invertTree(root.right);
        root.right= invertTree(tmp);
        return root;
    }
}
