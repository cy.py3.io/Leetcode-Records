package _173_Binary_Search_Tree_Iterator;

import java.util.LinkedList;
import java.util.Queue;

public class BSTIterator {
	public class TreeNode {
	    int val;
	    TreeNode left;
	    TreeNode right;
	    TreeNode(int x) { val = x; }
	 }
	
	private Queue<TreeNode> ans = new LinkedList<TreeNode>();
	
	public BSTIterator(TreeNode root) {

        midOrder(root);
    }
	
	public void midOrder(TreeNode root) {
		if(root==null) return;
		midOrder(root.left);
		ans.offer(root);
		midOrder(root.right);
	}

    /** @return whether we have a next smallest number */
    public boolean hasNext() {
        return !ans.isEmpty();
    }

    /** @return the next smallest number */
    public int next() {
        return ans.poll().val;
    }
}
