package _235_Lowest_Common_Ancestor_of_a_Binary_Search_Tree;

import _110_Balanced_Binary_Tree.Test2.TreeNode;

public class Test {
	public class TreeNode {
	    int val;
	    TreeNode left;
	    TreeNode right;
	    TreeNode(int x) { val = x; }
	}
	
	private TreeNode ans;
	private int flag = 0;
	
	public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        
    }
	
	private void dfs(TreeNode root,TreeNode p,TreeNode q) {
		if(root == null)return;
		
		if(root == p) flag++;
		if(root == q) flag++;
		if(flag == 2) {
			ans = root;
			flag++;
		}
		
	}
}



//public class Solution {
//    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
//        if(root.val > p.val && root.val > q.val){
//            return lowestCommonAncestor(root.left, p, q);
//        }else if(root.val < p.val && root.val < q.val){
//            return lowestCommonAncestor(root.right, p, q);
//        }else{
//            return root;
//        }
//    }
//}
